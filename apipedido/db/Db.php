<?php
class  Db {

	function conectar()
    {
        try {
	        $db = new PDO('mysql:host=localhost;dbname=pedidos',
	                $user='root',
	                $password='',
	                Array(
	        	        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                     
                        PDO::ERRMODE_WARNING
	                ));

            return  $db;

        } catch (Exception $e) {

            print "Error : ".$e->getMessage().'<br/>';
            die();

	    }
	}
}