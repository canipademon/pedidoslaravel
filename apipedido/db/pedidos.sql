-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.38-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Volcando estructura de base de datos para pedidos
CREATE DATABASE IF NOT EXISTS `pedidos` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `pedidos`;

-- Volcando estructura para tabla pedidos.clientes
CREATE TABLE IF NOT EXISTS `clientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(80) NOT NULL DEFAULT '',
  `nit` varchar(24) NOT NULL DEFAULT '',
  `tipoNit` int(1) NOT NULL DEFAULT '0',
  `direccion` varchar(100) NOT NULL DEFAULT '',
  KEY `Índice 1` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla pedidos.clientes: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` (`id`, `nombres`, `nit`, `tipoNit`, `direccion`) VALUES
	(1, 'Andres Martines Garzon', '255658888', 0, 'Calle 11 sur Bellavista'),
	(2, 'Arturo Gomez', '255658865', 0, 'Los Almendors carrera 12-24'),
	(3, 'Cristian Camilo Alvarez', '355858965', 0, 'Los olivos calle 52 # 48'),
	(4, 'Paola Fernandez', '559288556', 0, 'Baja playa avenida 21'),
	(5, 'Johana Eguiluz ', '255699668', 0, 'Via 40 Sao 72 Esquina Efrain ');
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;

-- Volcando estructura para tabla pedidos.pedidos
CREATE TABLE IF NOT EXISTS `pedidos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idCli` int(11) NOT NULL,
  `codigo` varchar(30) NOT NULL DEFAULT '',
  `estado` int(1) NOT NULL DEFAULT '0',
  `fechaEntrega` date NOT NULL,
  KEY `Índice 1` (`id`),
  KEY `Índice 2` (`idCli`),
  CONSTRAINT `FK_pedidos_clientes` FOREIGN KEY (`idCli`) REFERENCES `clientes` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla pedidos.pedidos: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `pedidos` DISABLE KEYS */;
INSERT INTO `pedidos` (`id`, `idCli`, `codigo`, `estado`, `fechaEntrega`) VALUES
	(1, 1, '01', 0, '2022-05-20'),
	(2, 2, '02', 0, '2022-05-20'),
	(3, 1, '03', 1, '2022-01-20'),
	(4, 5, '04', 1, '2022-02-15'),
	(5, 4, '05', 0, '2022-05-20');
/*!40000 ALTER TABLE `pedidos` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
