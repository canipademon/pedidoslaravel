<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: GET');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');
require 'db/Db.php';
require 'clientes/Clientes.php'; 
$adaptador = new Db();
$d  = new Clientes($adaptador);

if($_SERVER['REQUEST_METHOD']=='GET')
{
	$datos = $d->getClientes('',$_GET);

}
echo json_encode($datos);