<?php
class Clientes 
{
	function __construct($adaptador)
  {
    $this->db = $adaptador;
  }
  public function getClientes($con,$dt)
  {

    
    $sql = "select b.*,a.nombres as nomCli,
           a.direccion,
           case when b.estado = 0 then 'Pendiente' else 'Entregado'  end as estadoPed 
           from clientes a 
           inner join pedidos b on b.idCli = a.id 
           where b.codigo like '%".$dt['codigo']."%'  or
           ( a.nit like '%".$dt['nit']."%'
           and  a.tipoNit like '%".$dt['tipDoc']."%') 
           order by a.id desc";
   
    $tm = $this->db->conectar()->prepare($sql);
    //operations to get data from database
    $tm->execute();
    $datos =  $tm->fetchAll();

    foreach ($datos as $dat) {
        $data[] = array(
          'codigo' => $dat['codigo'],
          'nomCli' => $dat['nomCli'],
          'direccion' => $dat['direccion'],
          'estadoPed' => $dat['estadoPed'],
          'fechaEntrega' => $dat['fechaEntrega'],
        );
    }
    return $data;
  }
}